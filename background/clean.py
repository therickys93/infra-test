from minio import Minio
from minio.error import S3Error
import os
import time
from datetime import datetime, timedelta

def clean_older_files():
    client = Minio(
        os.getenv('MINIO_HOST'),
        access_key=os.getenv('MINIO_USER'),
        secret_key=os.getenv('MINIO_PASS'),
        secure=False,
    )

    found = client.bucket_exists(os.getenv('MINIO_BUCKET'))
    if not found:
        return
    files = client.list_objects(os.getenv('MINIO_BUCKET'))
    dt = datetime.now() - timedelta(days=1)
    for file in files:
        if dt.replace(tzinfo=None) > file.last_modified.replace(tzinfo=None):
            print(file.object_name + " is going to be deleted", flush=True)
            client.remove_object(os.getenv('MINIO_BUCKET'), file.object_name)

if __name__ == '__main__':
    while True:
        try:
            time.sleep(5)
            clean_older_files()
        except S3Error as exc:
            print("error occured.", exc, flush=True)
            break