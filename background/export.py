from minio import Minio
from minio.error import S3Error
import time
import os
import psycopg2
from psycopg2 import Error
from zipfile import ZipFile
import json
from datetime import datetime

def upload(filename):
    client = Minio(
        os.getenv('MINIO_HOST'),
        access_key=os.getenv('MINIO_USER'),
        secret_key=os.getenv('MINIO_PASS'),
        secure=False,
    )

    found = client.bucket_exists(os.getenv('MINIO_BUCKET'))
    if not found:
        client.make_bucket(os.getenv('MINIO_BUCKET'))
    else:
        print("Bucket '" + os.getenv('MINIO_BUCKET') + "' already exists", flush=True)

    client.fput_object(os.getenv('MINIO_BUCKET'), filename, filename)
    print("'" + filename + "' is successfully uploaded as object '" + filename + "' to bucket '" + os.getenv('MINIO_BUCKET') + "'.", flush=True)

def create_zip_file(zip_name, files=[]):
    with ZipFile(zip_name, 'w') as zip_file:
        for s_file in files:
            zip_file.write(s_file)

def dump_table_in_file(cursor, filename):
    cursor.execute("SELECT * FROM table_test")
    json_string = json.dumps(cursor.fetchall())
    with open(filename, "w") as file_open:
        file_open.write(json_string)

def execute_export(cursor):
    dt = datetime.now()
    str_date_time = dt.strftime("%Y_%m_%d_%H_%M_%S")
    test_table_file_name = 'export_test_table_' + str_date_time + '.json'
    zip_file_name = 'export_' + str_date_time + '.zip'
    dump_table_in_file(cursor, test_table_file_name)
    create_zip_file(zip_file_name, [test_table_file_name])
    upload(zip_file_name)
    os.remove(zip_file_name)
    os.remove(test_table_file_name)

def export():
    try:
        conn = psycopg2.connect(host=os.getenv('DB_HOST'), database=os.getenv('DB_DB'), user=os.getenv('DB_USER'), password=os.getenv('DB_PASS'))
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM public.status WHERE key = 'EXPORT';")
        record = cursor.fetchone()
        if record is not None:
            print(record, "\n", flush=True)
            if (record[2] == '1'):
                execute_export(cursor)
                cursor.execute("DELETE FROM public.status WHERE id = %s;", (record[0],))
                conn.commit()
        else:
            print("nothing to export", flush=True)
    except (Exception, Error) as error:
        print("Error while connecting to PostgreSQL", error, flush=True)
    finally:
        if (conn):
            cursor.close()
            conn.close()


if __name__ == "__main__":
    while True:
        try:
            time.sleep(5)
            export()
        except S3Error as exc:
            print("error occurred.", exc, flush=True)
            break
